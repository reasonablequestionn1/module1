﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            //main is empty
        }


        public int[] SwapItems(int a, int b)
        {
            return new int[] { b,a };
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];
            for(int i = 1; i < input.Length; ++i)            
                min = Math.Min(min, input[i]);
            return min;  
        }
    }
}
